use crate::message::Message;
use crate::message_codec::MessageCodec;
use crate::server::Server;
use futures::future::Future;
use futures::stream::Stream;
use futures::sync::mpsc;
use futures::{Async, Poll, Sink};
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::codec::Framed;
use tokio::io;
use tokio::net::TcpStream;
use tokio::prelude::stream::SplitSink;

pub(crate) type Tx = mpsc::UnboundedSender<Message>;
pub(crate) type Rx = mpsc::UnboundedReceiver<Message>;

struct RxMapErr(Rx);

impl Stream for RxMapErr {
    type Item = Message;
    type Error = io::Error;
    fn poll(&mut self) -> Result<Async<Option<Self::Item>>, Self::Error> {
        match self.0.poll() {
            Ok(v) => Ok(v),
            Err(_) => Err(io::Error::new(
                io::ErrorKind::Other,
                "Message channel error",
            )),
        }
    }
}

// TODO make it generic over stream type
type MessageFrame = Framed<TcpStream, MessageCodec>;
type MessageSink = SplitSink<MessageFrame>;
//type MessageStream = SplitStream<MessageFrame>;

struct PeerShared {
    addr: SocketAddr,
    server: Arc<Server>,
    // mutex state
}

impl PeerShared {
    fn process_message(&self, msg: Message) {
        self.server.post_message(self.addr, msg);
    }
}

pub(crate) struct Peer {
    shared: Arc<PeerShared>,
    send_all: Box<dyn Future<Item = (MessageSink, RxMapErr), Error = io::Error> + Send>,
    read_all: Box<dyn Future<Item = (), Error = io::Error> + Send>,
}

impl Peer {
    pub(crate) fn new(stream: TcpStream, server: Arc<Server>) -> Peer {
        let addr = stream.peer_addr().unwrap();
        let shared = Arc::new(PeerShared {
            addr,
            server: server.clone(),
        });

        let frame = Framed::new(stream, MessageCodec {});
        let (sink, stream) = frame.split();
        let (tx, rx) = mpsc::unbounded();

        let send_all = sink.send_all(RxMapErr(rx));

        let closure = {
            let shared = shared.clone();
            move |msg| {
                shared.process_message(msg);
                Ok(())
            }
        };
        let read_all = stream.for_each(closure);

        server.add_peer(addr, tx);
        Peer {
            shared,
            send_all: Box::new(send_all),
            read_all: Box::new(read_all),
        }
    }
}

impl Drop for Peer {
    fn drop(&mut self) {
        self.shared.server.remove_peer(&self.shared.addr);
    }
}

impl Future for Peer {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(), io::Error> {
        if !self.shared.server.running() {
            return Ok(Async::Ready(()));
        }

        // send messages
        match self.send_all.poll() {
            Ok(Async::NotReady) => {}
            Ok(Async::Ready(_)) => return Ok(Async::Ready(())),
            Err(e) => return Err(e),
        }

        // read messages
        match self.read_all.poll() {
            Ok(Async::NotReady) => {}
            Ok(Async::Ready(_)) => return Ok(Async::Ready(())),
            Err(e) => return Err(e),
        }

        Ok(Async::NotReady)
    }
}
