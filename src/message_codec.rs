use crate::message::{Message, MessageHeader};
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use bytes::{BufMut, BytesMut};
use std::io::Cursor;
use std::io::Write;
use tokio::codec::{Decoder, Encoder};
use tokio::io;

pub(crate) struct MessageCodec;

impl Encoder for MessageCodec {
    type Item = Message;
    type Error = io::Error;

    fn encode(&mut self, msg: Message, buf: &mut BytesMut) -> Result<(), Self::Error> {
        buf.reserve(msg.data.len() + MessageHeader::size_of());
        let mut writer = buf.writer();
        // write message header
        writer.write_u8(msg.header.category)?;
        writer.write_u8(msg.header.action)?;
        writer.write_u32::<LittleEndian>(msg.header.size)?;
        // write message data
        writer.write_all(&msg.data[..])?;
        Ok(())
    }
}

impl Decoder for MessageCodec {
    type Item = Message;
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        const HEADER_SIZE: usize = MessageHeader::size_of();
        if buf.len() < HEADER_SIZE {
            Ok(None)
        } else {
            let header = {
                // copy header bytes
                let mut header: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
                buf.iter().zip(header.iter_mut()).for_each(|(b, d)| *d = *b);
                // read header
                let mut cursor = Cursor::new(header);
                let category = cursor.read_u8()?;
                let action = cursor.read_u8()?;
                let size = cursor.read_u32::<LittleEndian>()?;
                MessageHeader {
                    category,
                    action,
                    size,
                }
            };
            if buf.len() >= HEADER_SIZE + header.size as usize {
                // consume header data
                let _ = buf.split_to(HEADER_SIZE);
                // consume message data
                let msg_buf = buf.split_to(header.size as usize);
                Ok(Some(Message {
                    header,
                    data: msg_buf.freeze(),
                }))
            } else {
                Ok(None)
            }
        }
    }
}
