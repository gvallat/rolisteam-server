use bytes::Bytes;

#[derive(Clone, Copy)]
pub(crate) struct MessageHeader {
    pub(crate) category: u8,
    pub(crate) action: u8,
    pub(crate) size: u32,
}

impl MessageHeader {
    pub(crate) const fn size_of() -> usize {
        std::mem::size_of::<u8>() * 2 + std::mem::size_of::<u32>()
    }
}

pub(crate) struct Message {
    pub(crate) header: MessageHeader,
    pub(crate) data: Bytes,
}
