use crate::server_config::ServerConfig;
use std::net::IpAddr;

#[derive(Default)]
pub struct ConnectionData<'a> {
    pub ip: Option<&'a IpAddr>,
    pub pwd: Option<&'a [u8]>,
}

pub trait ConnectionAccepter {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool;
}

#[derive(Default)]
pub struct AccepterList {
    accepters: Vec<Box<dyn ConnectionAccepter>>,
}

impl AccepterList {
    pub fn push<A: ConnectionAccepter + 'static>(&mut self, accepter: A) {
        self.accepters.push(Box::new(accepter));
    }
}

impl ConnectionAccepter for AccepterList {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool {
        self.accepters
            .iter()
            .all(|accepter| accepter.is_valid(data, config))
    }
}

#[derive(Default)]
pub struct IpRangeAccepter;
impl ConnectionAccepter for IpRangeAccepter {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool {
        if let Some(ip) = data.ip {
            config.is_ip_in_range(&ip)
        } else {
            false
        }
    }
}

#[derive(Default)]
pub struct IpBanAccepter;
impl ConnectionAccepter for IpBanAccepter {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool {
        if let Some(ip) = data.ip {
            config.is_ip_not_ban(&ip)
        } else {
            false
        }
    }
}

#[derive(Default)]
pub struct ServerPasswordAccepter;
impl ConnectionAccepter for ServerPasswordAccepter {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool {
        if let Some(pwd) = data.pwd {
            config.is_server_password_valid(pwd)
        } else {
            config.server_password.is_empty()
        }
    }
}

#[derive(Default)]
pub struct AdminPasswordAccepter;
impl ConnectionAccepter for AdminPasswordAccepter {
    fn is_valid(&self, data: &ConnectionData<'_>, config: &ServerConfig) -> bool {
        if let Some(pwd) = data.pwd {
            config.is_admin_password_valid(pwd)
        } else {
            config.admin_password.is_empty()
        }
    }
}

// TODO time accepter
