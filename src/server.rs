use crate::message::Message;
use crate::peer::Peer;
use crate::peer::Tx;
use crate::server_config::ServerConfig;
use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::atomic::AtomicBool;
use std::sync::Arc;
use std::sync::Mutex;
use tokio::net::TcpListener;
use tokio::net::TcpStream;
use tokio::prelude::*;

#[derive(Default)]
struct ServerState {
    peers: HashMap<SocketAddr, Tx>,
}

pub(crate) struct Server {
    config: ServerConfig,
    running: AtomicBool,
    state: Mutex<ServerState>,
}

impl Server {
    fn new(config: ServerConfig) -> Arc<Server> {
        Arc::new(Server {
            config,
            running: AtomicBool::new(true),
            state: Mutex::new(ServerState::default()),
        })
    }

    #[allow(unused)]
    fn config(&self) -> &ServerConfig {
        &self.config
    }

    pub(crate) fn running(&self) -> bool {
        self.running.load(std::sync::atomic::Ordering::SeqCst)
    }

    fn stop(&self) {
        self.running
            .store(false, std::sync::atomic::Ordering::SeqCst);
    }

    pub(crate) fn add_peer(&self, addr: SocketAddr, sender: Tx) {
        let mut guard = self.state.lock().unwrap();
        guard.peers.insert(addr, sender);
    }

    pub(crate) fn remove_peer(&self, addr: &SocketAddr) {
        let mut guard = self.state.lock().unwrap();
        guard.peers.remove(addr);
    }

    pub(crate) fn post_message(&self, _src_addr: SocketAddr, _msg: Message) {}
}

pub fn run_server(config: ServerConfig) -> std::io::Result<()> {
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), config.port);
    let server = Server::new(config);

    let listener = TcpListener::bind(&addr)?;

    let listing_future = listener
        .incoming()
        .take_while({
            let s = server.clone();
            move |_| Ok(s.running())
        })
        .map_err({
            let s = server.clone();
            move |e| {
                eprintln!("Incomming socket error: {}", e);
                s.stop();
            }
        })
        .for_each(move |stream| {
            process_incoming(stream, server.clone());
            Ok(())
        });

    tokio::run(listing_future);
    Ok(())
}

fn process_incoming(socket: TcpStream, server: Arc<Server>) {
    let peer = Peer::new(socket, server.clone());
    tokio::spawn(peer.map_err(|e| eprintln!("Peer error: {}", e)));
    // TODO select(peer, server.stopped()) to end futures
}
