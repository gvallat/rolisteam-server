use ipnetwork::IpNetwork;
use serde::Deserialize;
use std::net::IpAddr;

#[derive(Deserialize)]
pub struct ServerConfig {
    #[serde(rename = "port", default)]
    pub port: u16,

    #[serde(
        rename = "ServerPassword",
        default,
        deserialize_with = "deserialize_hash"
    )]
    pub server_password: Vec<u8>,

    #[serde(rename = "IpRange", deserialize_with = "deserialize_ip_network")]
    pub ip_range: Option<IpNetwork>,

    #[serde(rename = "IpBan", default, deserialize_with = "deserialize_ip_list")]
    pub ip_ban: Vec<IpAddr>,

    #[serde(rename = "ConnectionMax", default)]
    pub connection_max: String,

    #[serde(rename = "TimeStart", default)]
    pub time_start: String,

    #[serde(rename = "TimeEnd", default)]
    pub time_end: String,

    #[serde(rename = "IpMode", default)]
    pub ip_mode: String,

    #[serde(
        rename = "AdminPassword",
        default,
        deserialize_with = "deserialize_hash"
    )]
    pub admin_password: Vec<u8>,

    #[serde(rename = "ThreadCount", default)]
    pub thread_count: u32,

    #[serde(rename = "ChannelCount", default)]
    pub channel_count: u32,

    #[serde(rename = "TimeToRetry", default)]
    pub time_to_retry: u32,

    #[serde(rename = "TryCount", default)]
    pub try_count: u32,

    #[serde(rename = "LogLevel", default)]
    pub log_level: u8, // TODO Add LogLevel enum

    #[serde(
        rename = "MaxMemorySize",
        default,
        deserialize_with = "deserialize_size"
    )]
    pub max_memory_size: u64,

    #[serde(rename = "DeepInspectionLog", default)]
    pub deep_inspection_log: bool,

    #[serde(rename = "LogFile", default)]
    pub log_file: String,
}

fn deserialize_hash<'de, D: serde::Deserializer<'de>>(
    deserializer: D,
) -> Result<Vec<u8>, D::Error> {
    use b64::FromBase64;
    let hash = String::deserialize(deserializer)?;
    hash.from_base64().map_err(serde::de::Error::custom)
}

fn deserialize_ip_network<'de, D: serde::Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<IpNetwork>, D::Error> {
    let ip_network_str = String::deserialize(deserializer)?;
    let ip_network: IpNetwork = ip_network_str.parse().map_err(serde::de::Error::custom)?;
    Ok(Some(ip_network))
}

fn deserialize_ip_list<'de, D: serde::Deserializer<'de>>(
    deserializer: D,
) -> Result<Vec<IpAddr>, D::Error> {
    let list = String::deserialize(deserializer)?;
    let mut ips = Vec::new();
    for ip_str in list.split(',') {
        let ip = ip_str.parse().map_err(serde::de::Error::custom)?;
        ips.push(ip);
    }
    Ok(ips)
}

fn deserialize_size<'de, D: serde::Deserializer<'de>>(deserializer: D) -> Result<u64, D::Error> {
    let size_str = String::deserialize(deserializer)?;
    if size_str.ends_with('G') {
        let size_base: u64 = size_str[0..size_str.len() - 1]
            .parse()
            .map_err(serde::de::Error::custom)?;
        Ok(size_base * 1024 * 1024 * 1024)
    } else if size_str.ends_with('M') {
        let size_base: u64 = size_str[0..size_str.len() - 1]
            .parse()
            .map_err(serde::de::Error::custom)?;
        Ok(size_base * 1024 * 1024)
    } else {
        Ok(0)
    }
}

impl ServerConfig {
    pub fn read<P: AsRef<std::path::Path>>(path: P) -> std::io::Result<ServerConfig> {
        let mut file = std::fs::File::open(path)?;
        let mut data = String::new();
        use std::io::Read;
        file.read_to_string(&mut data)?;
        let mut deserializer = serde_ini::Deserializer::from_str(&data);
        ServerConfig::deserialize(&mut deserializer)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    }

    pub fn is_ip_in_range(&self, ip: &IpAddr) -> bool {
        if let Some(ip_range) = self.ip_range {
            ip_range.contains(*ip)
        } else {
            true
        }
    }

    pub fn is_ip_not_ban(&self, current_ip: &IpAddr) -> bool {
        self.ip_ban.iter().all(|ip| *current_ip != *ip)
    }

    pub fn is_server_password_valid(&self, pwd: &[u8]) -> bool {
        self.server_password == pwd
    }

    pub fn is_admin_password_valid(&self, pwd: &[u8]) -> bool {
        self.admin_password == pwd
    }
}
