use rolisteam_server::*;

fn main() {
    let config = match ServerConfig::read("config.ini") {
        Ok(config) => config,
        Err(e) => {
            eprintln!("Could not read configuration file: {}", e);
            return;
        }
    };

    if let Err(e) = run_server(config) {
        eprintln!("Error while running server: {}", e);
    }
}
