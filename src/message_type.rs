use crate::message::MessageHeader;
use num_traits::FromPrimitive;

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum AdminAction {
    EndConnection = 0,
    Heartbeat,
    ConnectionInfo,
    Goodbye,
    Kicked,
    MoveChannel,
    SetChannelList,
    NeedPassword,
    AuthentificationSucessed,
    AuthentificationFail,
    LockChannel,
    JoinChannel,
    DeleteChannel,
    AddChannel,
    ChannelPassword,
    BanUser,
    ClearTable,
    AdminPassword,
    AdminAuthSucessed,
    AdminAuthFail,
    MovedIntoChannel,
    GMStatus,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum PlayerAction {
    PlayerConnection = 0,
    DelPlayer,
    ChangePlayerName,
    ChangePlayerColor,
    ChangePlayerAvatar,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum CharacterPlayerAction {
    AddPlayerCharacter = 0,
    DelPlayerCharacter,
    ToggleViewPlayerCharacter,
    ChangePlayerCharacterSize,
    ChangePlayerCharacterName,
    ChangePlayerCharacterColor,
    ChangePlayerCharacterAvatar,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum NpcAction {
    AddNpc = 0,
    DelNpc,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum CharacterAction {
    AddCharacterList = 0,
    MoveCharacter,
    ChangeCharacterState,
    ChangeCharacterOrientation,
    ShowCharecterOrientation,
    AddCharacterSheet,
    UpdateFieldCharacterSheet,
    CloseCharacterSheet,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum MapAction {
    AddEmpty = 0,
    Load,
    Import,
    Close,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum PaitingAction {
    Pen = 0,
    Line,
    EmptyRectangle,
    FilledRectangle,
    EmptyEllipse,
    FilledEllipse,
    Text,
    Hand,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum ChatAction {
    ChatMessage = 0,
    DiceMessage,
    EmoteMessage,
    UpdateChat,
    DelChat,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum MusicAction {
    Stop = 0,
    Play,
    Pause,
    New,
    ChangePosition,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum SetupAction {
    AddFeature = 2,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum SharedPreferenceAction {
    AddDiceAlias = 0,
    MoveDiceAlias,
    RemoveDiceAlias,
    AddState,
    MoveState,
    RemoveState,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum VMapAction {
    AddVmap = 0,
    VmapChanges,
    LoadVmap,
    CloseVmap,
    AddItem,
    DelItem,
    MoveItem,
    ZValueItem,
    RotationItem,
    RectGeometryItem,
    DelPoint,
    OpacityItemChanged,
    LayerItemChanged,
    GeometryItemChanged,
    AddPoint,
    GeometryViewChanged,
    CharacterStateChanged,
    CharacterChanged,
    SetParentItem,
    MovePoint,
    VisionChanged,
    ColorChanged,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum MediaAction {
    Add = 0,
    Close,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum SharedNoteAction {
    UpdateTextAndPermission = 2,
    UpdateText,
    UpdatePermissionOneUser,
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive)]
enum WebPageAction {
    UpdateContent = 5,
}

enum MessageType {
    Administration(AdminAction),
    Player(PlayerAction),
    CharacterPlayer(CharacterPlayerAction),
    NPC(NpcAction),
    Character(CharacterAction),
    Draw(PaitingAction),
    Map(MapAction),
    Chat(ChatAction),
    Music(MusicAction),
    Setup(SetupAction),
    SharePreferences(SharedPreferenceAction),
    VMap(VMapAction),
    Media(MediaAction),
    SharedNote(SharedNoteAction),
    WebPage(WebPageAction),
}

impl MessageType {
    #[allow(unused)]
    fn try_from_message_header(header: MessageHeader) -> Option<MessageType> {
        let action = header.action;
        match header.category {
            0 => Some(MessageType::Administration(AdminAction::from_u8(action)?)),
            1 => Some(MessageType::Player(PlayerAction::from_u8(action)?)),
            2 => Some(MessageType::CharacterPlayer(
                CharacterPlayerAction::from_u8(action)?,
            )),
            3 => Some(MessageType::NPC(NpcAction::from_u8(action)?)),
            4 => Some(MessageType::Character(CharacterAction::from_u8(action)?)),
            5 => Some(MessageType::Draw(PaitingAction::from_u8(action)?)),
            6 => Some(MessageType::Map(MapAction::from_u8(action)?)),
            7 => Some(MessageType::Chat(ChatAction::from_u8(action)?)),
            8 => Some(MessageType::Music(MusicAction::from_u8(action)?)),
            9 => Some(MessageType::Setup(SetupAction::from_u8(action)?)),
            10 => Some(MessageType::SharePreferences(
                SharedPreferenceAction::from_u8(action)?,
            )),
            11 => Some(MessageType::VMap(VMapAction::from_u8(action)?)),
            12 => Some(MessageType::Media(MediaAction::from_u8(action)?)),
            13 => Some(MessageType::SharedNote(SharedNoteAction::from_u8(action)?)),
            14 => Some(MessageType::WebPage(WebPageAction::from_u8(action)?)),
            _ => None,
        }
    }
}
